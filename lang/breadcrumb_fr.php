<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Module: sarkaspip
// Langue: fr

return [
	'abonnement_newsletter' => 'Abonnement',
	'agenda' => 'Agenda',
	'breve' => 'Brèves',
	'galerie' => 'Galerie',
	'mot' => 'Mots-clés',
	'herbier' => 'Sites favoris',
	'site' => 'Sur le web',
	'401' => 'Page non autorisée',
	'404' => 'Page introuvable',
	'plan' => 'Plan du site',
	'recherche' => 'Recherche',
	'newsletter_subscribe' => 'Inscription à la Newsletter',
];
